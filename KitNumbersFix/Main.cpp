#include "plugin-std.h"
#include <string>
#include <vector>
#include <map>
#include <fstream>
#include <sstream>
#include "myutils.h"

using namespace plugin;
using namespace std;

enum PlayerPos {
    POS_GK,
    POS_SW,
    POS_RWB,
    POS_RB,
    POS_RCB,
    POS_CB,
    POS_LCB,
    POS_LB,
    POS_LWB,
    POS_RDM,
    POS_RCDM,
    POS_CDM,
    POS_LCDM,
    POS_LDM,
    POS_RWM,
    POS_RM,
    POS_RCM,
    POS_CM,
    POS_LCM,
    POS_LM,
    POS_LWM,
    POS_RAM,
    POS_RCAM,
    POS_CAM,
    POS_LCAM,
    POS_LAM,
    POS_RF,
    POS_CF,
    POS_LF,
    POS_RS,
    POS_ST,
    POS_LS,
    POS_SUB,
    POS_RES,
    POS_LCF,
    POS_RCF
};

unsigned int gTransferAddr1[3] = { 0, 0, 0 };
unsigned int gTransferAddr2[3] = { 0, 0, 0 };
unsigned int gNumber = 2;

map<unsigned int, vector<unsigned char>> &PreferredNumbers() {
    static map<unsigned int, vector<unsigned char>> preferredNumbers;
    return preferredNumbers;
}

void ReadPreferredNumbers() {
    ifstream f("data\\cmn\\db_extensions\\preferred_numbers.txt");
    if (f.is_open()) {
        for (string line; getline(f, line); ) {
            auto parts = Split(line, ',');
            if (parts.size() > 1) {
                int playerId = SafeConvertInt<int>(parts[0]);
                if (playerId > 0) {
                    vector<unsigned char> numbers;
                    for (size_t i = 1; i < parts.size(); i++) {
                        unsigned char number = SafeConvertInt<unsigned char>(parts[i]);
                        if (number >= 1 && number <= 99)
                            numbers.push_back(number);
                    }
                    if (!numbers.empty())
                        PreferredNumbers()[playerId] = numbers;
                }
            }
        }
    }
}

unsigned int GetPreferredJerseyNumber(int teamId, int playerId) {
    unsigned int result = 2;
    if (teamId != 0 && playerId != 0) {
        vector<bool> numbers(99, false);
        char condAttr[0x1C];
        char cond[0x24];
        unsigned int table;
        unsigned int entries;
        char entry[0x10];
        CallMethod<0x418C00>(condAttr, "teamid"); // SYSTEM::SMALL_STRING::SMALL_STRING
        Call<0x5B8410>(cond, condAttr, teamId); // Redux::Database::AttribEquals<int>::AttribEquals<int>
        void *dbi = CallAndReturn<void *, 0x491340>(); // GetFifaDbInterface
        CallMethod<0x5B9330>(dbi, &table, "teamplayerlinks"); // DbInterface::BaseTable
        CallMethod<0x5BE590>(&table, "players", 0, 0, 0, 0, 0, 0, 0); // DbDataQuery::OtherTables
        CallMethod<0x5B9A30>(&table, cond); // DbDataQuery::Filter
        CallMethod<0x5BECA0>(&table, &entries); // DbDataQuery::GetEntries
        CallMethod<0x5BED00>(&table); // DbDataQuery::~DbDataQuery
        CallMethod<0x5B8D80>(cond, 0); // Redux::Database::AttribEquals<int>::~AttribEquals<int>
        CallMethod<0x418CD0>(condAttr); // SYSTEM::SMALL_STRING::~SMALL_STRING
        for (size_t i = 0; i < CallMethodAndReturn<unsigned int, 0x5B7CC0>(entries); i++) { // DbEntriesInternal::Size
            CallMethod<0x5B7E10>(entries, entry, i); // DbEntriesInternal::Entry
            int number = CallMethodAndReturn<int, 0x418FF0>(entry, "jerseynumber"); // DbEntry::Attribute
            if (number >= 1 && number <= 99)
                numbers[number - 1] = true;
            CallMethod<0x418C80>(entry); // DbEntry::~DbEntry
        }
        CallMethod<0x418F10>(entries, entries); // DbEntriesInternal::destructor
        int position = -1;
        CallMethod<0x418C00>(condAttr, "playerid"); // SYSTEM::SMALL_STRING::SMALL_STRING
        Call<0x5B8410>(cond, condAttr, playerId); // Redux::Database::AttribEquals<int>::AttribEquals<int>
        CallMethod<0x5B9330>(dbi, &table, "players"); // DbInterface::BaseTable
        CallMethod<0x5B9A30>(&table, cond); // DbDataQuery::Filter
        CallMethod<0x5BECA0>(&table, &entries); // DbDataQuery::GetEntries
        CallMethod<0x5BED00>(&table); // DbDataQuery::~DbDataQuery
        CallMethod<0x5B8D80>(cond, 0); // Redux::Database::AttribEquals<int>::~AttribEquals<int>
        CallMethod<0x418CD0>(condAttr); // SYSTEM::SMALL_STRING::~SMALL_STRING
        if (CallMethodAndReturn<unsigned int, 0x5B7CC0>(entries) > 0) { // DbEntriesInternal::Size
            CallMethod<0x5B7E10>(entries, entry, 0); // DbEntriesInternal::Entry
            position = CallMethodAndReturn<int, 0x418FF0>(entry, "preferredposition1"); // DbEntry::Attribute
            CallMethod<0x418C80>(entry); // DbEntry::~DbEntry
        }
        auto GetNextAvailableNumber = [](vector<unsigned char> const &priorityNumbers, vector<unsigned char> const &priorityNumbersGeneral, vector<bool> &numbers) -> unsigned char {
            for (unsigned char i : priorityNumbers) {
                if (!numbers[i - 1])
                    return i;
            }
            for (unsigned char i : priorityNumbersGeneral) {
                if (!numbers[i - 1])
                    return i;
            }
            for (unsigned char i = 12; i <= 99; i++) {
                if (!numbers[i - 1])
                    return i;
            }
            return 0;
        };
        vector<unsigned char> priority;
        if (PreferredNumbers().contains(playerId))
            priority = PreferredNumbers()[playerId];
        if (position == POS_GK)
            result = GetNextAvailableNumber(priority, { 1, 12, 25, 23, 13, 99 }, numbers);
        else if (position == POS_RB || position == POS_RWB)
            result = GetNextAvailableNumber(priority, { 2, 4, 3, 6, 5 }, numbers);
        else if (position == POS_LB || position == POS_LWB)
            result = GetNextAvailableNumber(priority, { 3, 5, 2, 6, 4 }, numbers);
        else if (position == POS_CB || position == POS_RCB || position == POS_LCB || position == POS_SW)
            result = GetNextAvailableNumber(priority, { 3, 4, 5, 2, 6 }, numbers);
        else if (position == POS_CDM || position == POS_RDM || position == POS_LDM || position == POS_RCDM || position == POS_LCDM)
            result = GetNextAvailableNumber(priority, { 4, 6, 5, 7 }, numbers);
        else if (position == POS_CM || position == POS_RCM || position == POS_LCM)
            result = GetNextAvailableNumber(priority, { 6, 8, 7, 5 }, numbers);
        else if (position == POS_RM || position == POS_LM)
            result = GetNextAvailableNumber(priority, { 7, 11, 10, 8, 6, 17 }, numbers);
        else if (position == POS_CAM || position == POS_RAM || position == POS_LAM || position == POS_RCAM || position == POS_LCAM)
            result = GetNextAvailableNumber(priority, { 10, 7, 11, 8, 6, 20 }, numbers);
        else if (position == POS_RWM || position == POS_LWM)
            result = GetNextAvailableNumber(priority, { 7, 10, 11, 9, 8, 17 }, numbers);
        else if (position == POS_ST || position == POS_CF || position == POS_RF || position == POS_LF || position == POS_RS || position == POS_LS)
            result = GetNextAvailableNumber(priority, { 9, 10, 7, 11, 8, 19 }, numbers);
        else
            result = GetNextAvailableNumber(priority, {}, numbers);
    }
    if (result == 0)
        result = 2;
    return result;
}

void __declspec(naked) SetJerseyNumber_Case1() {
    __asm mov eax, gNumber
    __asm mov[esp + 0x180], eax
    __asm mov eax, 0x52ADD4
    __asm jmp eax
}

void __declspec(naked) SetJerseyNumber_Case2() {
    __asm mov eax, gNumber
    __asm mov[esp + 0x98], eax
    __asm mov eax, 0x5244F0
    __asm jmp eax
}

void __declspec(naked) SetJerseyNumber_Case3() {
    __asm mov eax, gNumber
    __asm mov[esp + 0x98], eax
    __asm mov eax, 0x524307
    __asm jmp eax
}

template<unsigned int Addr>
void OnTransfer1(int teamId, int playerId, int year) {
    gNumber = GetPreferredJerseyNumber(teamId, playerId);
    CallDynGlobal(gTransferAddr1[Addr], teamId, playerId, year);
    gNumber = 2;
}

template<unsigned int Addr>
bool OnTransfer2(int playerId, int teamId, int a3, int duration) {
    gNumber = GetPreferredJerseyNumber(teamId, playerId);
    bool result = CallAndReturnDynGlobal<bool>(gTransferAddr2[Addr], playerId, teamId, duration);
    gNumber = 2;
    return result;
}

class JerseyNumberFix {
public:
    JerseyNumberFix() {
        auto v = FM::GetAppVersion();
        if (v.id() == ID_FIFA_07_1100_C) {
            patch::RedirectJump(0x52ADC9, SetJerseyNumber_Case1);
            patch::RedirectJump(0x5244E5, SetJerseyNumber_Case2);
            patch::RedirectJump(0x5242FC, SetJerseyNumber_Case3);
            gTransferAddr1[0] = patch::RedirectCall(0x53BCE2, OnTransfer1<0>);
            gTransferAddr1[1] = patch::RedirectCall(0x53C2FC, OnTransfer1<1>);
            gTransferAddr1[2] = patch::RedirectCall(0x54C7FB, OnTransfer1<2>);
            gTransferAddr2[0] = patch::RedirectCall(0x52469F, OnTransfer2<0>);
            gTransferAddr2[1] = patch::RedirectCall(0x524AF7, OnTransfer2<1>);
            gTransferAddr2[2] = patch::RedirectCall(0x524BD6, OnTransfer2<2>);
            ReadPreferredNumbers();
        }
    }
} jerseyNumberFix;
